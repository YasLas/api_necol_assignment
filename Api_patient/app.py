from flask import Flask
from flask_restx import Api, Resource, fields
from models import db, Patient
from config import Config

app = Flask(__name__)
app.config.from_object(Config)

# Initialize database with Flask app (if using a database)
db.init_app(app)

# Setup API with Flask-Restx
api = Api(app, version='1.0', title='Patient API', description='A simple Patient Management API')

# Define a namespace
ns = api.namespace('patients', description='Patient operations')

# Define the model for a Patient (use fields from your models.Patient class)
patient_model = api.model('Patient', {
    'id': fields.Integer(readOnly=True, description='The patient unique identifier'),
    'name': fields.String(required=True, description='The patient name'),
    # Add other fields here
})

@ns.route('/')
class PatientList(Resource):
    @ns.doc('list_patients')
    @ns.marshal_list_with(patient_model)
    def get(self):
        # Logic to list all patients
        return []

    @ns.doc('create_patient')
    @ns.expect(patient_model)
    @ns.marshal_with(patient_model, code=201)
    def post(self):
        # Logic to create a new patient
        return api.payload, 201

@ns.route('/<int:id>')
@ns.response(404, 'Patient not found')
@ns.param('id', 'The patient identifier')
class Patient(Resource):
    @ns.doc('get_patient')
    @ns.marshal_with(patient_model)
    def get(self, id):
        # Logic to get a patient by id
        return {}

    @ns.doc('delete_patient')
    @ns.response(204, 'Patient deleted')
    def delete(self, id):
        # Logic to delete a patient
        return '', 204

    @ns.doc('update_patient')
    @ns.expect(patient_model)
    @ns.marshal_with(patient_model)
    def put(self, id):
        # Logic to update a patient
        return api.payload

if __name__ == '__main__':
    app.run(debug=True)
