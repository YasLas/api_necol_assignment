from flask import Flask
from .config import Config

def create_app():
    app = Flask(__name__)
    app.config.from_object(Config)

    # Initialize other components like database here

    with app.app_context():
        # Import parts of our application
        from .app import routes

    return app
